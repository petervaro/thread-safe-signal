use std::{
    fmt::{
        self,
        Debug,
        Display,
        Formatter,
    },
    sync::{
        Arc,
        atomic::{
            AtomicBool,
            Ordering,
        },
    },
};


/*----------------------------------------------------------------------------*/
pub struct Signal(Arc<AtomicBool>);


/*----------------------------------------------------------------------------*/
impl Signal
{
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    pub fn clone(signal: &Self) -> Self
    {
        Self(Arc::clone(&signal.0))
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    pub fn get(&self) -> bool
    {
        self.0.load(Ordering::Relaxed)
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    pub fn set(&mut self, value: bool)
    {
        self.0.store(value, Ordering::Relaxed);
    }

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    pub fn take(&mut self) -> bool
    {
        self.0.swap(false, Ordering::Relaxed)
    }
}


/*----------------------------------------------------------------------------*/
impl From<bool> for Signal
{
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    fn from(value: bool) -> Self
    {
        Self(Arc::new(AtomicBool::new(value)))
    }
}


/*----------------------------------------------------------------------------*/
impl Default for Signal
{
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    fn default() -> Self
    {
        false.into()
    }
}


/*----------------------------------------------------------------------------*/
impl Debug for Signal
{
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result
    {
        write!(formatter, "Signal({})", self)
    }
}


/*----------------------------------------------------------------------------*/
impl Display for Signal
{
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result
    {
        write!(formatter, "{}", self.get())
    }
}
